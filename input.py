dict1 = {
    "A":{
        "general":{
            "age": 45,
            "name": "John"
        },
        "additional":{
            "first_commit":{
                "list_of attempts": [
                    {
                        "type1": "main",
                        "type" : "extra"
                    },
                    {
                        "data": 6
                    }
                ]
            }
        },
        "car_data":{
            "general":{
                "type": "mercedes",
                "model": "GLE"
            }
        }
    }
}

dict2 = {
    "A":{
        "general":{
            "age": 45,
            "name": "John"
        },
        "car_data":{
            "general":{
                "type": "mercedes",
                "model": "GLE"
                
            }
        },
        "additional":{
            "first_commit":{
                "list_of attempts": [
                    {
                        "type1": "main",
                        "type" : "extra"
                    },
                    {
                        "data": 5
                    }
                ]
            }
        }
    }
}
