from input import *
import json


def compare_obj(obj1, obj2):

    if isinstance(obj1, dict) and isinstance(obj2, dict):
        for key in obj1:
            if key not in obj2:
                print("key--",key, "not present in the obj2")
            else:
                if type(obj1[key]) not in (dict, list):
                    if obj1[key] != obj2[key]:
                        print("values for key---", key, "in both the object is different")
                else:
                    compare_obj(obj1[key], obj2[key])

        for key_2 in obj2:
            if key_2 not in obj1:
                print("key--", key_2, "not present in obj1")

    elif isinstance(obj1, list) and isinstance(obj2, list):
        for i in range (len(obj1)):
            compare_obj(obj1[i], obj2[i])        
    else:
        if obj1 != obj2:
            print("value", obj2, "is not present" )


if __name__ == "__main__":
    obj1 = (json.dumps(dict1, sort_keys=True))
    obj2 = (json.dumps(dict2, sort_keys=True))

    obj1 = json.loads(obj1)
    obj2 = json.loads(obj2)

    if (obj1 == obj2):
        print("Both the objects are equal")
    else:
        compare_obj(obj1, obj2)












